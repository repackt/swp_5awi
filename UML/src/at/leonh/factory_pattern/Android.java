package at.leonh.factory_pattern;

public class Android implements OS{

	public void spec() {
		System.out.println("Android: 200 MHz processor, 32 MB of RAM, and 32 MB of storage");
	}

}
