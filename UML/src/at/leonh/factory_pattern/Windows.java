package at.leonh.factory_pattern;

public class Windows implements OS {

	public void spec() {
		System.out.println("Windows: 1 GHz processor, 1/2 GB of RAM, and 16/20 GB of storage, RAM and storage size depend on 32-Bit or 64-Bit OS");

	}

}
