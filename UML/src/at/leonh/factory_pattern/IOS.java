package at.leonh.factory_pattern;

public class IOS implements OS {

	public void spec() {
		System.out.println("IOS: 412 MHz processor, 128 MB of RAM, and 1337 MB of storage");

	}

}
