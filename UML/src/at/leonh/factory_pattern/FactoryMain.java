package at.leonh.factory_pattern;

public class FactoryMain {

	public static void main(String[] args) {
		OsFactory osf = new OsFactory();
		OS object = osf.getInstance("Open");
		object.spec();

	}

}
