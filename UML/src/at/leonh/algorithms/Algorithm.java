package at.leonh.algorithms;

public interface Algorithm {
	public int[] doSort(int[] myArray);
}
