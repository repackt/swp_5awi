package at.leonh.algorithms;

import java.util.ArrayList;
import java.util.Collections;

public class main {

	public static void main(String[] args) {
		ArrayList<Integer> ranarray = new ArrayList<Integer>();
		ranarray.add(1);
		ranarray.add(2);
		ranarray.add(3);
		ranarray.add(4);
		ranarray.add(5);
		ranarray.add(6);
		ranarray.add(7);

		System.out.println("Original List: " + ranarray);
		Collections.shuffle(ranarray);
		System.out.println("Random List: " + ranarray);

	}
}
