package at.leonh.tictactoe;

import java.util.Scanner;

public class TicTacToe {

	private int x;
	private int y;
	public static boolean playing;

	public TicTacToe(String[][] field) {

		int Player1;
		while (playing==true) {
			boolean isValid = false;
			while (isValid) {
				System.out.println("Please enter playername: " + Player1);
				Scanner input =new Scanner(System.in);
				String input = Scanner.next();
				String[] values = input.split(",");
				x = Integer.parseInt(values[0]);
				y = Integer.parseInt(values[1]);
				if (field[x][y] == "\u0000") {
					isValid = true;
				} else {
					System.out.println("This Field has already been chosen. Please select another Field");
				}
			}
		}
		if (Player1 == 1) {
			field[x][y] = "o";
		} else {
			field[x][y] = "x";
		}
	}

	public static void main(String[] args) {
		// Einstieg in�s Spiel
		playing=true;
	}

}
