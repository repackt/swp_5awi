package at.leonh.CD_PLayer;

public interface Playable {
	public void play();

	public void pause();

	public void stop();

}
