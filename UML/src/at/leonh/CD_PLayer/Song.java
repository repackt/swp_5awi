package at.leonh.CD_PLayer;

public class Song implements Playable {

	private String title;
	private int length;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	public void sayHello() {
		System.out.println("Hello!");
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Song is playing.");
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		System.out.println("Song paused.");

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		System.out.println("Song stopped.");
	}

}
