package at.leonh.CD_PLayer;

import java.util.List;

public class Actor {

	String firstName;
	String lastName;
	List<CD> cds;

	public Actor(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	private List<CD> getAllRecords() {
		return cds;
	}

}
