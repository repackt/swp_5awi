package at.leonh.CD_PLayer;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Actor a1 = new Actor("Morty", "Bonk");
		Actor a2 = new Actor("Rick", "Donk");

		List<CD> CDs = new ArrayList<CD>();
		CD cd1 = new CD("Rise");
		CD cd2 = new CD("Into the Dark");

		CDs.add(cd1);
		CDs.add(cd2);

		List<DVD> DVDs = new ArrayList<DVD>();
		DVD dvd1 = new DVD("Best of Rise Against");
		DVD dvd2 = new DVD("Best of Rick Donk");

		DVDs.add(dvd1);
		DVDs.add(dvd2);

		for (CD cd : CDs) {
			System.out.println(cd.getName());
		}
	}
}
