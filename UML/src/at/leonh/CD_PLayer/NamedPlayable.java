package at.leonh.CD_PLayer;

public abstract class NamedPlayable implements Playable {

	private String title;
	private int length;
	
	public NamedPlayable() {
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}

	public String getTitle() {
		return title;
	}

	public int getLength() {
		return length;
	}

}