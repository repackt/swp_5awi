package at.leonh.observer_pattern;

import java.util.ArrayList;
import java.util.List;

public class Sensor {
	private List<Observer> observables = new ArrayList<Observer>();

	public Sensor() {
		// TODO Auto-generated constructor stub
		this.observables = new ArrayList<Observer>();
	}

	public void addObserver(Observer obs) {
		// TODO Auto-generated method stub
		observables.add(obs);
	}

	public void informAll(String status) {
		// TODO Auto-generated method stub
		for (Observer obs : observables) {
			obs.inform(status);
		}
	}

}
