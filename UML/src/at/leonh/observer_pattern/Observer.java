package at.leonh.observer_pattern;

public interface Observer {
	public void inform(String status);
}
