package at.leonh.observer_pattern;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Sensor sensor = new Sensor();

		Lantern lan1 = new Lantern();
		Lantern lan2 = new Lantern();

		Christmas christ1 = new Christmas();

		sensor.addObserver(lan1);
		sensor.addObserver(lan2);
		sensor.addObserver(christ1);
		sensor.informAll("yeet");

	}

}
