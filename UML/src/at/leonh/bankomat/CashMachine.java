package at.leonh.bankomat;

import java.util.Scanner;

public class CashMachine {

	public static boolean isRunning;

	static void cashMachine() {

		double balance = 0;
		int selection;
		double deposit;
		double withdrawal;

		while (isRunning = true) {

			System.out.println(
					"Please select an action by entering one of the following numbers: " + "\n" + "1 - Deposit" + "\n"
							+ "2 - Withdraw" + "\n" + "3 - Check current balance" + "\n" + "4 - Stop machine" + "\n");
			Scanner in = new Scanner(System.in);
			selection = in.nextInt();

			if (selection == 1) {
				System.out.println("Please enter the amount you want to deposit." + "\n");
				Scanner amount = new Scanner(System.in);
				deposit = amount.nextDouble();
				balance = balance + deposit;
				System.out.println("You have deposited " + deposit + " to your bank account." + "\n"
						+ "Your current balance is: " + balance + "\n");
				// break;
			}

			if (selection == 2) {
				System.out.println("Please enter the amount you want to withdraw." + "\n");
				Scanner amount = new Scanner(System.in);
				withdrawal = amount.nextDouble();
				if ((balance - withdrawal) >= 0) {
					balance = balance - withdrawal;
					System.out.println("You have withdrawn " + withdrawal + " from your bank-account." + "\n"
							+ "Your current balance is: " + balance + "\n");
					// break;
				} else
					System.out.println("Sorry, you can't withdraw more than your account-balance." + "\n"
							+ "Your current balance is: " + balance + "\n");
				// break;
			}

			if (selection == 3) {
				System.out.println("Your current balance is: " + balance + "\n");
				// break;
			}

			if (selection == 4) {
				System.out.println("Machine is stopping!");
				isRunning = false;
				break;
			}
		}
	}

	public static void main(String[] args) {
		System.out.println("ATM is starting");
		isRunning = true;
		cashMachine();
	}
}
