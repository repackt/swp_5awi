package at.leonh.encryption;

import java.util.Arrays;

public class main {

	public static void main(String[] args) {
		char[] original = { 'h', 'e', 'y' };
		int verschub = 14;

		// Caesar.encrypt(original, verschub);
		char[] encrypted = Caesar.encrypt(original, verschub);
		System.out.println(Arrays.toString(encrypted));

		char[] decrypted = Caesar.decrypt(encrypted, verschub);
		System.out.println(Arrays.toString(decrypted));
	}

}
