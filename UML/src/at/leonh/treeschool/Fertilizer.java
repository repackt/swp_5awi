package at.leonh.treeschool;

public interface Fertilizer {
	public void fertilize();
}
