package at.leonh.treeschool;

public abstract class Tree {
	private int maxSize;
	private int maxDiameter;
	private Fertilizer fertilizeProcess;

	public Tree(int maxSize, int maxDiameter, Fertilizer fertilizeProcess) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertilizeProcess = fertilizeProcess;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}

	public Fertilizer getFertilizeProcess() {
		return fertilizeProcess;
	}

	public void setFertilizeProcess(Fertilizer fertilizeProcess) {
		this.fertilizeProcess = fertilizeProcess;
	}

}
